# WBD Lismore Transcoder

Small php script to transcodes from Lismore City Council GIS format into What Bin Day Import format

### Prerequisites
- A GIS file output from Lismore GIS system
- PHP installed

### Running as CLI under PHP
- Modify config.php based on your input file name
- Run `php transcode.php >out.json`
- `out.json` can be imported into What Bin Day

### Running over CURL
-- curl -v -F cli=1 -F csv=@small.csv -D /dev/stdout http://localhost:9000 

### Running as Server
- Modify config.php based on your input file name
- Run `php -S localhost:9000` or similar for your local env
- Open http://localhost:9000 in your browser

#### To test
Pipe the output from transcode to the test file:
`php transcode.php | php test.php`

### For help
support@whatbinday.com