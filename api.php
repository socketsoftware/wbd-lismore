<?php
        include_once('transcode.php');

        $errors= array();
        $files = scandir_chrono('rules', true, ['json']);
        $rulefile = '';
        if (isset($_POST['ruleset'])) {
            $rulefile = $_POST['ruleset'];
            if ($rulefile == 'default') {
                $rulefile = reset($files);
            }
        } else {
            $rulefile = reset($files);
        }
        $realprocessruleset = 'rules/' . $rulefile;

        if (!file_exists($realprocessruleset)) {
            $errors[]='Ruleset ' . $rulefile . ' not found';
        }

       if(isset($_FILES['csv'])) {
            $file_name = $_FILES['csv']['name'];
            $file_size =$_FILES['csv']['size'];
            $file_tmp =$_FILES['csv']['tmp_name'];
            $file_type=$_FILES['csv']['type'];
            $nmeexp = explode('.',$_FILES['csv']['name']);
            $file_ext=strtolower(end($nmeexp));
       } else {
            $errors[]="CSV not found";
       }
        
        $extensions= array("csv");
        
        if(in_array($file_ext, $extensions)=== false){
            $errors[]="Extension not allowed, please choose a CSV";
        }
        
        if($file_size > 6291456){
            $errors[]='File size must be less than 6Mb';
        }
      
        if(empty($errors)==true){
            $new_dir = "uploads/" . date('Ymd') . '-' . generateRandomString(10);
            $new_name = $new_dir . '/upload.csv';
            $out_name = $new_dir . '/out.jcsv';
            mkdir($new_dir);
            move_uploaded_file($file_tmp, $new_name);
            $out_file = fopen($out_name, 'w');
            $err_count = generate($new_name, $out_file, $realprocessruleset);
            fclose($out_file);
            $host = $_SERVER['HTTP_HOST'];
            $url = get_current_url() . '/' . $out_name;
            if (str_contains($host, 'whatbinday')) {
                $url = "https://whatbinday.com/lismore/transcoder/" . $out_name;
            }
            header('Content-Type: application/json; charset=utf-8');
            $obj = (object) array('success' => true, 'generated' => $url, 'error_count' => $err_count);
            echo json_encode($obj, JSON_UNESCAPED_SLASHES);
        } else {
            header('Content-Type: application/json; charset=utf-8');
            http_response_code(400);
            $obj = (object) array('success' => false, 'errors' => $errors);
            echo json_encode($obj);
        }
?>