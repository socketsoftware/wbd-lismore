import ast
import csv
import time
import json
import urllib
from pathlib import Path
import datetime
import urllib.request

script_location = Path(__file__).absolute().parent
file_location = script_location / '2023-test-sm.csv'
out_location = script_location / 'out.csv'

cntr = 1
extras = ["Status", "Parsed", "URL"]
writer = None
with open(out_location, "w", newline='') as of:
    with open(file_location, "r") as f:
        reader = csv.DictReader(f)
        for row in reader:

            if (cntr == 1):
                fields = list(row.keys())
                fields.extend(extras)
                writer = csv.DictWriter(of, fields, delimiter=',')
                print(f"Writing headers {fields}")
                writer.writeheader()

            print(f"Processing line {cntr} with data {row}")
            unit = row["Unit"]
            parsed_unit = ""
            if unit and unit.isdigit():
                parsed_unit = f"Unit {unit}"
            formatted = f"{parsed_unit} {row['House']} {row['Street_Name']}, {row['Suburb']} NSW {row['PostCode']}".strip()
            json_row = {
                "address": {
                    "street_number": row["House"],
                    "route": row["Street_Name"],
                    "locality": row["Suburb"],
                    "administrative_area_level_1": "New South Wales",
                    "postal_code": row["PostCode"],
                    "part": parsed_unit,
                    "subpremise":"",
                    "formatted_address": formatted
                },
                "geometry":{"location":{"lat":0,"lng":0}
                }}
            json_address = json.dumps(json_row)
            eaddress = urllib.request.pathname2url(f"{json_address}")
            search = f'https://api.whatbinday.com/api/searchbare?apiKey=fef3003f-1321-4de3-ae49-392e22458fb4&address={eaddress}&agendaResultLimit=3&dateFormat=EEEE%20dd%20MMM%20yyyy&generalBinImage=&recycleBinImage=&greenBinImage=&glassBinImage=&paperBinImage=&displayFormat=calendar&calendarStart=&calendarFutureMonths=12&calendarPrintBannerImgUrl=&notes='
            esearch = urllib.request.pathname2url(f"{search}")
            complete = f'https://print.whatbinday.com/calendar?url={esearch}&css=https://api.whatbinday.com/api/css'
            k = datetime.datetime.now().strftime("%Y%m%d%H%M%S")
            outdf = script_location / 'out' / f'{k}.pdf'

            try:
                r = urllib.request.urlretrieve(complete, outdf)
                row["Status"] = "OK"
                row["Parsed"] = json_address
                row["URL"] = complete
                writer.writerow(row)
                print ("ok")
            except (urllib.error.URLError, urllib.error.HTTPError) as e:
                row["Status"] = f"Error {e.reason}"
                row["Parsed"] = json_address
                row["URL"] = complete
                writer.writerow(row)
                print ("error")
            cntr += 1
            time.sleep(1)
        print("done")