import os
from PyPDF2 import PdfMerger
from pathlib import Path

script_location = Path(__file__).absolute().parent

def merge_pdfs_in_folder(folder_path, output_filename):
    # Initialize the PDF merger
    merger = PdfMerger()

    # List all files in the folder
    files = os.listdir(folder_path)

    # Filter for PDF files and sort them (optional)
    pdf_files = sorted([f for f in files if f.endswith('.pdf')])

    count = 1
    # Merge each PDF filepip3
    for pdf in pdf_files:
        pdf_path = os.path.join(folder_path, pdf)
        merger.append(pdf_path)
        count = count + 1
        # if count == 100:
        #     output_path = os.path.join(folder_path, f"{output_filename}{count}.pdf")
        #     merger.write(output_path)
        #     merger.close()
        #     merger = PdfMerger()

    # Write out the merged PDF
    output_path = os.path.join(folder_path, output_filename)
    merger.write(output_path)
    merger.close()

    print(f'Merged {len(pdf_files)} PDF files into {output_filename}')

# Example usage
folder_path = script_location / 'out'
output_filename = 'joined.pdf'
merge_pdfs_in_folder(folder_path, output_filename)