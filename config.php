<?php 

class Constants
{
    //input file name
    const INPUT_FILE_NAME = '2023-test.csv';
    const USE_STREET_NUMBER_PARTS = true;
    const OUTPUT_ERRORS = true;

    //column header names
    const COLUMN_NAME_SUBURB = 'Suburb';
    const COLUMN_NAME_STREET_NAME = 'Street_Name';
    const COLUMN_NAME_STREET_NUMBER = 'House';
    const COLUMN_NAME_STREET_UNIT = 'Unit';
    const COLUMN_NAME_SERVICE = 'Garb_Serv';
    const COLUMN_NAME_SERVICE_DESC = 'Waste_Desc';
}

function get_postcode($sub) {
    $postcodes = array(
        "BEXHILL" => 2480,
        "BLAKEBROOK" => 2480,
        "BLUE KNOB" => 2480,
        "BOAT HARBOUR" => 0,
        "BOOERIE CREEK" => 2480,
        "BOOYONG" => 2480,
        "BROADWATER" => 2472,
        "BUCKENDOON" => 2472,
        "CANIABA" => 2480,
        "CHILCOTTS GRASS" => 2480,
        "CLUNES" => 2480,
        "COFFEE CAMP" => 2480,
        "CORNDALE" => 2480,
        "DORROUGHBY" => 2480,
        "DUNGARUBBA" => 2480,
        "DUNOON" => 2480,
        "EAST CORAKI" => 2471,
        "EAST LISMORE" => 2480,
        "ELTHAM" => 2480,
        "GEORGICA" => 2480,
        "GIRARDS HILL" => 2480,
        "GOOLMANGAR" => 2480,
        "GOONELLABAH" => 2480,
        "GREEN FOREST" => 2471,
        "HOWARDS GRASS" => 2480,
        "JIGGI" => 2480,
        "KEERRONG" => 2480,
        "KILGIN" => 2472,
        "KOONORIGAN" => 2480,
        "LAGOON GRASS" => 2480,
        "LARNOOK" => 2480,
        "LEYCESTER" => 2480,
        "LINDENDALE" => 2480,
        "LISMORE" => 2480,
        "LISMORE HEIGHTS" => 2480,
        "LOFTVILLE" => 2480,
        "MAROM CREEK" => 2480,
        "MCLEANS RIDGES" => 2480,
        "MODANVILLE" => 2480,
        "MONALTRIE" => 2480,
        "NIMBIN" => 2480,
        "NORTH LISMORE" => 2480,
        "NORTH WOODBURN" => 2471,
        "NUMULGI" => 2480,
        "PEARCES CREEK" => 2477,
        "REPENTANCE CREEK" => 2480,
        "RICHMOND HILL" => 2480,
        "ROCK VALLEY" => 2480,
        "ROSEBANK" => 2480,
        "RUTHVEN" => 2480,
        "SOUTH GUNDURIMBA" => 2480,
        "SOUTH LISMORE" => 2480,
        "STONY CHUTE" => 2480,
        "TERANIA CREEK" => 2480,
        "THE CHANNON" => 2480,
        "TREGEAGLE" => 2480,
        "TUCKI TUCKI" => 2480,
        "TUCKURIMBA" => 2480,
        "TULLERA" => 2480,
        "TUNCESTER" => 2480,
        "TUNTABLE CREEK" => 2480,
        "WHIAN WHIAN" => 2480,
        "WOODLAWN" => 2480,
        "WYRALLAH" => 2480);
    if (array_key_exists($sub, $postcodes)) {
        return $postcodes[$sub];
    } else {
        return 0;
    }
}