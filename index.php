<?php
   include_once('transcode.php');
?>
<?php if(!isset($_POST['cli'])){ ?>
<html>
   <head>
      <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
      <style>
         .green {
            background: #edffe8 !important;
         }
         .recycle {
            background: #fffbbf !important;
         }
         .waste {
            background: #fad7d2 !important;
         }       
         h4 {
            margin-top: 40px;
         }
         body {
            padding-top: 5rem;
         }
         
      </style>
   </head>
   <body margin="20px">
   <main role="main" class="container">
      <h2>Lismore File Transcoder</h2>

      <?php
         $files = scandir_chrono('rules', true, ['json']);
         $rulefile = '';
         if (isset($_POST['ruleset'])) {
            $rulefile = $_POST['ruleset'];
         } else {
            $rulefile = reset($files);
         }
         $realrulefile = 'rules/' . $rulefile;
      ?>
      <form action="" method="POST" enctype="multipart/form-data">
            Change Rulset:
            <select name="ruleset">
               <?php 
                  foreach($files as $file) {
                     $selected = ($rulefile == $file) ? 'selected' : '';
                     echo '<option ' . $selected . ' value="' . $file . '">' . $file . '</option>';
                  }
               ?>
            </select>
            <input type="submit" value="Change Ruleset">
      </form>
      <hr/>
      <form action="" method="POST" enctype="multipart/form-data">
         <p>
            <input type="file" name="csv" />
         </p>
         <p>
            Ruleset used: <?php echo $rulefile; ?>
            <input type=hidden name="processruleset" value="<?php echo $rulefile; ?>"/>
         </p>
         <p>
            <input type="submit" value="Process"/>
         </p>
      </form>
      <?php
   }

   if(isset($_FILES['csv'])){

      $processruleset = $_POST['processruleset'];
      $realprocessruleset = 'rules/' . $processruleset;
      if (!file_exists($realprocessruleset)) {
         die('Ruleset not found');
      }

      $errors= array();
      $file_name = $_FILES['csv']['name'];
      $file_size =$_FILES['csv']['size'];
      $file_tmp =$_FILES['csv']['tmp_name'];
      $file_type=$_FILES['csv']['type'];
      $nmeexp = explode('.',$_FILES['csv']['name']);
      $file_ext=strtolower(end($nmeexp));
      
      $extensions= array("csv");
      
      if(in_array($file_ext, $extensions)=== false){
         $errors[]="Extension not allowed, please choose a CSV";
      }
      
      if($file_size > 6291456){
         $errors[]='File size must be less than 6Mb';
      }
      
      if(empty($errors)==true){
         $new_dir = "uploads/" . date('Ymd') . '-' . generateRandomString(10);
         $new_name = $new_dir . '/upload.csv';
         $out_name = $new_dir . '/out.jcsv';
         mkdir($new_dir);
         move_uploaded_file($file_tmp, $new_name);
         $out_file = fopen($out_name, 'w');
         $err_count = generate($new_name, $out_file, $realprocessruleset);
         fclose($out_file);
         
         if(isset($_POST['cli'])) {
            header("Content-Type: text/plain");
            header("Content-Length:" . filesize($out_name));
            header("Content-Disposition: attachment; filename=out.json");
            header("x-wbd-error-count: " . strval($err_count));
            readfile($out_name);
         } else {
            echo "<a href='" . $out_name . "'>Generated File</a> (right-click, save link as)<br/>There were " . $err_count . " errors<br/><br/>";
         }

      } else {
         if(isset($_POST['cli'])) {
            http_response_code(400);
            print_r($errors);
         } else {
            print_r($errors);
         }      
      }
   }
?>
<?php
   // Read the JSON file 
   $json = file_get_contents($realrulefile);
   
   // Decode the JSON file
   $rules = json_decode($json,true);

   function safeval($item, $key) {
      return $item[$key];
   }
?>         
         <div>
            <h4>Business Rules for <?php echo basename($rulefile); ?></h4>
            <p>Rules are processed in order and the first matching rule is used.  To match a business rule the data in the Waste_Desc and Garb_Serv columns of the uploaded file row must contain the text in matching columns for the rule (case insensitive).</p>
            <p>Your can also <a href="rules/<?php echo basename($rulefile); ?>">download</a> these roles (right-click save as).</p>
         </div>
         <form action="rules.php" method="POST">
            <table class="table">
            <thead>
            <tr>
               <th>#</th>   
               <th>Waste_Desc</th>   
               <th>Garb_Serv</th>
               <th>Waste Date</th>
               <th>Waste Cadence</th>
               <th>Recycle Date</th>
               <th>Recycle Cadence</th>
               <th>Green Date</th>
               <th>Green Cadence</th>
            </tr>
            </thead>
            <tbody>
            <?php
               $i = 0;
               $display_rules = insert_blank_rules($rules);
               foreach($display_rules as $rule) { //foreach element in $arr
                  echo '<tr>';
                  echo '<td>' . ++$i . '.<input type="hidden" name="rule_' . $i . '"/></td>';
                  echo '<td><input type="text" name="column_desc_' . $i . '" size="25" value="' . safeval($rule, "column_desc") . '"></td>';
                  echo '<td><input type="text" name="column_garb_' . $i . '" size="25" value="' . safeval($rule, "column_garb") . '"></td>';
                  echo '<td class="waste"><input type="text" name="waste_start_' . $i . '" size="10" value="' . safeval($rule, "waste_start") . '"></td>';
                  echo '<td class="waste"><input type="text" name="waste_cadence_' . $i . '" size="5" value="' . safeval($rule, "waste_cadence") . '"></td>';
                  echo '<td class="recycle"><input type="text" name="recycle_start_' . $i . '" size="10" value="' . safeval($rule, "recycle_start") . '"></td>';
                  echo '<td class="recycle"><input type="text" name="recycle_cadence_' . $i . '" size="5" value="' . safeval($rule, "recycle_cadence") . '"></td>';
                  echo '<td class="green"><input type="text" name="green_start_' . $i . '" size="10" value="' . safeval($rule, "green_start") . '"></td>';
                  echo '<td class="green"><input type="text" name="green_cadence_' . $i . '" size="5" value="' . safeval($rule, "green_cadence") . '"></td>';
                  echo '</tr>';
               }
            ?>
            <?php for ($j = 0; $j < 5; $j++) { ?>
               <tr>
                  <td><?php echo ++$i; ?><input type="hidden" name="rule_<?php echo $i; ?>"/></td>
                  <td><input type="text" name="column_desc_<?php echo $i; ?>" size="25" value=""></td>
                  <td><input type="text" name="column_garb_<?php echo $i; ?>" size="25" value=""></td>
                  <td class="waste"><input type="text" name="waste_start_<?php echo $i; ?>" size="10" value=""></td>
                  <td class="waste"><input type="text" name="waste_cadence_<?php echo $i; ?>" size="5" value=""></td>
                  <td class="recycle"><input type="text" name="recycle_start_<?php echo $i; ?>" size="10" value=""></td>
                  <td class="recycle"><input type="text" name="recycle_cadence_<?php echo $i; ?>" size="5" value=""></td>
                  <td class="green"><input type="text" name="green_start_<?php echo $i; ?>" size="10" value=""></td>
                  <td class="green"><input type="text" name="green_cadence_<?php echo $i; ?>" size="5" value=""></td>
               </tr>
            <?php } ?>
            </tbody>
            </table>
            <p>
               Label: <input type="text" name="label" value="" size="50" placeholder="Provide a label for these rules"> <input type="submit" name="Save Rules" value="Save Rules">
            </p>
         </form>
   </main>
   </body>
   </html>