<?php

//helper function to check for substring
if (!function_exists('str_contains')) {
    function str_contains($haystack, $needle) {
        return strpos($haystack, $needle) !== FALSE;
    }
}

//add or remove days from a date
//$date [Date]
//$days [Int] 7 or -7
//return [Date]
function day_math($date, $days) {
    if (is_string($date)) {
        $d = strtotime($date);
    } else {
        $d = $date;
    }
    return date('Y-m-d', strtotime($days .' days', $d));
}

//from an epoch calcuate walk backwards until a specific day of the week is found
//$dow [Int] Day of week, Monday is 1
//$epoch [DateString] eg '2020-01-01'
//return [Date]
function get_last_day($dow, $epoch = '2021-01-30') {
    $d = strtotime($epoch);
    $m = date('w', $d);
    while ($m != $dow) {
        $d = strtotime('-1 days', $d);
        $m = date('w', $d);
    }
    return $d;
}

//from a string look for days based on their full name and return the previous day
//matching that day of the week
//$data [String] eg. 'Pickup bins on MONDAY'
//$epoch [DateString] eg '2020-01-01'
//return [Date] or 0 if cannot find a matching day in the text
function get_dow($data, $epoch = '2021-01-30') {
    $d = strtoupper($data);
    if (str_contains($d, 'MONDAY')) {
        return get_last_day(1, $epoch);
    } else if (str_contains($d, 'TUESDAY')) {
        return get_last_day(2, $epoch);
    } else if (str_contains($d, 'WEDNESDAY')) {
        return get_last_day(3, $epoch);
    } else if (str_contains($d, 'THURSDAY')) {
        return get_last_day(4, $epoch);
    } else if (str_contains($d, 'FRIDAY')) {
        return get_last_day(5, $epoch);
    } else {
        return 0;
    }
}

//Check a value in an array
//$record [Array]
//$key [Mixed] Key of to lookup a value in the record
//return [Boolean] TRUE if key exists in array and is not null or empty
function valid_field($record, $key) {
    return array_key_exists($key, $record) && $record[$key] != null && $record[$key] != '';
}



function insert_blank_rules($rules) {
    $newrules = [];
    foreach($rules as $rule) {
        $newrules[] = [
            'column_desc' => '',
            'column_garb' => '',
            'waste_start' => '',
            'waste_cadence' => '',
            'recycle_start' => '',
            'recycle_cadence' => '',
            'green_start' => '',
            'green_cadence' => ''
        ];
        $newrules[] = $rule;
    }
    return $newrules;
}


function generateRandomString($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

/**
 * Return an array of file paths representing the contents of the target
 * directory, ordered by date instead of by filename.
 * 
 * @param string $path The target directory path
 * @param bool $reverse Whether to sort in reverse date order (oldest first)
 * @param array $exts If set, only find files with these extensions
 * @return array A sorted array of absolute filesystem paths
 */
function scandir_chrono(string $path, bool $reverse = false, ?array $exts = []): array {

    /* Fail if the directory can't be opened */
    if (!(is_dir($path) && $dir = opendir($path))) {
        return [];
    }

    /* An array to hold the results */
    $files = [];

    while (($file = readdir($dir)) !== false) {
        /* Skip anything that's not a regular file */
        if (filetype($path . '/' . $file) !== 'file') {
            continue;
        }
        /* If extensions were provided and this file doesn't match, skip it */
        if (!empty($exts) && !in_array(pathinfo($path . '/' . $file,
                                PATHINFO_EXTENSION), $exts)) {
            continue;
        }
        /* Add this file to the array with its modification time as the key */
        $files[filemtime($path . '/' . $file)] = $file;
    }
    closedir($dir);

    /* Sort and return the array */
    $fn = $reverse ? 'krsort' : 'ksort';
    $fn($files);
    return $files;
}


function get_current_url() {
  $protocol = (!empty($_SERVER['HTTPS']) && (strtolower($_SERVER['HTTPS']) == 'on' || $_SERVER['HTTPS'] == '1')) ? 'https://' : 'http://';
  $port = $_SERVER['SERVER_PORT'] ? ':'.$_SERVER['SERVER_PORT'] : '';
  return $protocol.$_SERVER['SERVER_NAME'].$port;
}