#!/usr/bin/env bash

#input file to transcode in CSV format
input_file="small.csv"

#temporary output file from transcoding process
output_file="out.json"

#API upload variables see https://console.whatbinday.com/portal/help#toc19
wbd_access_key="" #TODO
wbd_secret="" #TODO
wbd_desription="Auto $(date '+%Y-%m-%d')"
wbd_state="New South Wales" #use full state name
wbd_region="Region"
wbd_email="youremail@example.com"
wbd_purge="false"

#the ruleset to use, specific ruleset name or 'default' to use the latest
ruleset="default"

transcode_url="http://localhost:8080/api.php"
#transcode_url="https://whatbinday.com/lismore/transcoder/api.php"

echo "Transcoding $input_file on $transcode_url and saving to $output_file"

#call the transcoder service to convert out input file to the WBD format.
if curl -F "ruleset=$ruleset" -F "csv=@$input_file" -o "$output_file" "$transcode_url"; then

    success=`cat "$output_file" | jq -r '.success'`

    if [[ "$success" == "true" ]]; then

        #get the error count from the response response header.
        error_cnt=`cat "$output_file" | jq -r '.error_count'`

        echo "Error count is $error_cnt"
        #TODO determine if error_cnt is appropriate for your use case, i.e you may expect a certain % of errors for example

        #extract the generated file location from the json response
        trasncoded_file_url=`cat "$output_file" | jq -r '.generated'`
        echo "Transcoded file url is: $trasncoded_file_url"
        echo "Uploading to WBD.  Purge value is $wbd_purge"
        curl -X POST --header "X-WBD-Access-Key: $wbd_access_key" \
            --header "X-WBD-Access-Secret: $wbd_secret" \
            --header "X-WBD-Batch-Upload-Format: Detect" \
            --header "X-WBD-Batch-Description: $wbd_desription" \
            --header "X-WBD-Batch-State: $wbd_state" \
            --header "X-WBD-Batch-Region: $wbd_region" \
            --header "X-WBD-Batch-Email: $wbd_email" \
            --header "X-WBD-Batch-Url: $trasncoded_file_url" \
            --header "X-WBD-Batch-Purge-Existing: $wbd_purge" \
            https://api.whatbinday.com/V1/BatchUpload
    else 
        echo "Transcoding was unsuccessful, check ($output_file) for details"
    fi
else 
    echo "Error transcoding"
fi