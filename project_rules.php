<?php

include_once 'library.php';

function project($day, $cadence, $until) {
    if (strlen($day)== 0) return "";
    $start = new DateTime($day);
    $end = new DateTime($until);
    while ($start < $end) {
        $start = $start->add(new DateInterval('P' . $cadence . 'D'));
    } 
    return $start->format('Y-m-d');
}

$rules_raw = file_get_contents('rules/rules.json');
$rules = json_decode($rules_raw, true);
$util = '2023-01-01';
$newrules = array();

foreach($rules as $rule) {
    $rule["waste_start"] = project($rule["waste_start"], $rule["waste_cadence"], $util);
    $rule["green_start"] = project($rule["green_start"], $rule["green_cadence"], $util);
    $rule["recycle_start"] = project($rule["recycle_start"], $rule["recycle_cadence"], $util);
    $newrules[] = $rule;
}

$d = new DateTime();
rename('rules/rules.json', 'rules/rules' . $d->format('Ymd') . '-' . generateRandomString(4) . '.json');
$f = json_encode($newrules, JSON_PRETTY_PRINT);
file_put_contents('rules/rules.json', $f);
