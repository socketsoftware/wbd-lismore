<?php

    $label = isset($_POST['label']) ? $_POST['label'] : die('missing label field');
    $label = preg_replace("/[^a-zA-Z0-9]+/", "", $label);
    if (strlen($label) == 0) die('missing label field (after stripping illegal characters)');

    $i = 1;
    $rule = array();
    while (true) {
        if (isset($_POST['rule_' . $i])) {
            $rule['column_desc'] = $_POST['column_desc_' . $i];
            $rule['column_garb'] = $_POST['column_garb_' . $i];
            $rule['waste_start'] = $_POST['waste_start_' . $i];
            $rule['waste_cadence'] = $_POST['waste_cadence_' . $i];
            $rule['recycle_start'] = $_POST['recycle_start_' . $i];
            $rule['recycle_cadence'] = $_POST['recycle_cadence_' . $i];
            $rule['green_start'] = $_POST['green_start_' . $i];
            $rule['green_cadence'] = $_POST['green_cadence_' . $i];
            
            $rule['column_desc'] = preg_replace("/[^a-zA-Z0-9 \-]+/", "", $rule['column_desc']);
            $rule['column_garb'] = preg_replace("/[^a-zA-Z0-9 \-]+/", "", $rule['column_garb']);
            $rule['waste_start'] = preg_replace("/[^0-9\-]+/", "", $rule['waste_start']);
            $rule['waste_cadence'] = preg_replace("/[^0-9]+/", "", $rule['waste_cadence']);
            $rule['recycle_start'] = preg_replace("/[^0-9\-]+/", "", $rule['recycle_start']);
            $rule['recycle_cadence'] = preg_replace("/[^0-9]+/", "", $rule['recycle_cadence']);
            $rule['green_start'] = preg_replace("/[^0-9\-]+/", "", $rule['green_start']);
            $rule['green_cadence'] = preg_replace("/[^0-9]+/", "", $rule['green_cadence']);

            if (strlen($rule['column_desc']) != 0 && strlen($rule['column_garb']) != 0) {
                $rules[] = $rule;
            }
            $i++;
        } else {
            break;
        }
    }

    if (count($rules) == 0) die('No rules found');

    $data = json_encode($rules, JSON_PRETTY_PRINT);
    $filename = 'rules_' . date('YmdHis') . '-' . $label . '.json';

    file_put_contents('rules/' . $filename, $data);

    echo 'Rules saved to ' . $filename;
    echo '<br><a href="index.php">Return to main page</a>';