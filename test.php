<?php
//Uses a series of regexes to validate that the correct schedule has been output
//The below has been cross checked with Lismore's current website

$matches = array(
    "Integrated Week 2 Monday" => "/Acacia Avenue.*28.*WasteBin.*2021-01-11.*Interval.*14.*GreenBin.*2021-01-11.*Interval.*7.*RecycleBin.*2021-01-04.*Interval.*14/",
    "Integrated Week 1 Tuesday" => "/Avondale Avenue.*82.*WasteBin.*2021-01-05.*Interval.*14.*GreenBin.*2021-01-12.*Interval.*7.*RecycleBin.*2021-01-12.*Interval.*14/",
    "Integrated Week 2 Wednesday" => "/Bangalow Road.*158.*WasteBin.*2021-01-13.*Interval.*14.*GreenBin.*2021-01-13.*Interval.*7.*RecycleBin.*2021-01-06.*Interval.*14/",
    "Integrated Week 1 Friday" => "/Robinson Avenue.*25.*WasteBin.*2021-01-08.*Interval.*14.*GreenBin.*2021-01-15.*Interval.*7.*RecycleBin.*2021-01-15.*Interval.*14/",
    "Integrated Plus Week 1 Monday" => "/Rosegum Drive.*6.*WasteBin.*2021-01-04.*Interval.*14.*GreenBin.*2021-01-11.*Interval.*7.*RecycleBin.*2021-01-11.*Interval.*14/",
    
    "Urban Half Monday Week 2" => "/Rosella Chase.*15\".*WasteBin.*2021-01-25.*Interval.*28.*GreenBin.*2021-01-18.*Interval.*14.*RecycleBin.*2021-01-04.*Interval.*28/",
    "Urban Half Thursday Week 1" => "/Rous Road.*66\".*WasteBin.*2021-01-07.*Interval.*28.*GreenBin.*2021-01-14.*Interval.*14.*RecycleBin.*2021-01-28.*Interval.*28/",
    
    "Premium Thursday Week 2" => "/Andrews Crescent.*\"7\".*WasteBin.*2021-01-07.*Interval.*7.*GreenBin.*2021-01-07.*Interval.*7.*RecycleBin.*2021-01-07.*Interval.*14/",
    "Premium Tuesday Week 1" => "/Aurora Street.*\"35\".*WasteBin.*2021-01-12.*Interval.*7.*GreenBin.*2021-01-12.*Interval.*7.*RecycleBin.*2021-01-12.*Interval.*14/",
    
    "Rural Wednesday" => "/Baldock Drive.*\"1\".*WasteBin.*2021-01-06.*Interval.*7.*RecycleBin.*2021-01-06.*Interval.*14/",

    "Rural Village Half" => "/Cecil Street.*\"13\".*WasteBin.*2021-01-14.*Interval.*28.*GreenBin.*2021-01-21.*Interval.*14.*RecycleBin.*2021-01-07.*Interval.*28/",

    "Rural Half Wednesday" => "/Breckenridge Street.*\"95\".*WasteBin.*2021-01-13.*Interval.*14.*RecycleBin.*2021-01-06.*Interval.*28/",
    "Rural Half Village Tuesday" => "/Dunoon Road.*\"316\".*WasteBin.*2021-01-05.*Interval.*14.*RecycleBin.*2021-01-26.*Interval.*28/",

    "Rural Village Tuesday" => "/Dunoon Road.*\"340\".*WasteBin.*2021-01-05.*Interval.*14.*GreenBin.*2021-01-05.*Interval.*7.*RecycleBin.*2021-01-12.*Interval.*14/",
    "Rural Village Friday" => "/Gardenia Crescent.*\"8\".*WasteBin.*2021-01-08.*Interval.*14.*GreenBin.*2021-01-08.*Interval.*7.*RecycleBin.*2021-01-15.*Interval.*14/",

    "Rural Business Tuesday" => "/James Gibson Road.*\"406\".*WasteBin.*2021-01-05.*Interval.*7.*RecycleBin.*2021-01-12.*Interval.*14/",
);

echo 'Found ' . count($matches) . " tests\n";

$file = file("php://stdin");
$cntr = 0;
foreach ($file as $row) {
    foreach ($matches as $key => $regexp) {
        preg_match_all($regexp, $row, $keys, PREG_PATTERN_ORDER);
        if (count($keys[0]) > 0) {
            echo "SUCCESS Found Match [\n\t" . $key . "\n\t" . $row . "]\n";
            $matches = array_diff($matches, array($key => $regexp));
            $cntr++;
        }
    }
}

echo "\n\nMatches not found:\n";
foreach ($matches as $key => $regexp) {
    echo $regex . "\n";
    foreach ($file as $row) {
        foreach ($matches as $key => $regexp) {
            $broken = explode(".*", $regexp);
            $broader = $broken[0] . ".*" . $broken[1] . ".*" . $broken[2] . "/";
            preg_match_all($broader, $row, $keys, PREG_PATTERN_ORDER);
            if (count($keys[0]) > 0) {
                echo "Expected [\n\t" . $regexp . "\n]\n";
                echo "We Got [\n\t" . $key . "\n\t" . $row . "]\n";
                break;
            }
        }
    }
}

if ($cntr == count($matches)) {
    echo "\n\nALL TESTS SUCCESS\n";
} else {
    echo "\n\nERRORS FOUND\n";
}