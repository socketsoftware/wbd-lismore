<?php
// Transcodes from Lismore City Council GIS format into What Bin Day Import format

include_once('config.php');
include_once('library.php');

//Validate a row for required information
//$record [Array]
//return [String], null if success or a string for the first error found
function validate_row($record) {
    if (!valid_field($record, 'Suburb')) {
        return 'Invalid Suburb';
    }
    if (!valid_field($record, 'Street')) {
        return 'Invalid Street';
    }
    if (!valid_field($record, 'Number')) {
        return 'Invalid Number';
    }
    if (count($record['Pickups']) == 0) {
        return 'No Pickups found';
    }
    return null;
}

//Validate an input record for correct fields
//$value [Array]
//return true if valid
function validate_input($value, $line, $output_file) {

    if (!array_key_exists(Constants::COLUMN_NAME_SUBURB, $value)) {
        fwrite($output_file, "# Error line " . $line . ' no column ' . Constants::COLUMN_NAME_SUBURB);
        return false;
    }
    if (!array_key_exists(Constants::COLUMN_NAME_STREET_NAME, $value)) {
        fwrite($output_file, "# Error line " . $line . ' no column ' . Constants::COLUMN_NAME_STREET_NAME);
        return false;
    }
    if (!array_key_exists(Constants::COLUMN_NAME_STREET_NUMBER, $value)) {
        fwrite($output_file, "# Error line " . $line . ' no column ' . Constants::COLUMN_NAME_STREET_NUMBER);
        return false;
    }
    if (!array_key_exists(Constants::COLUMN_NAME_SERVICE, $value)) {
        fwrite($output_file, "# Error line " . $line . ' no column ' . Constants::COLUMN_NAME_SERVICE);
        return false;
    }
    if (!array_key_exists(Constants::COLUMN_NAME_SERVICE_DESC, $value)) {
        fwrite($output_file, "# Error line " . $line . ' no column ' . Constants::COLUMN_NAME_SERVICE_DESC);
        return false;
    }

    if (Constants::USE_STREET_NUMBER_PARTS) {
        if (!array_key_exists(Constants::COLUMN_NAME_STREET_UNIT, $value)) {
            fwrite($output_file, "# Error line " . $line . ' no column ' . Constants::COLUMN_NAME_STREET_UNIT);
            return false;
        }
    }

    return true;
}

//Convert an imported record into an array of WBD Pickups
//This function correct as at Feb 2021, subsequent years may require a bit of adjustment
//Lismore has quite a few different pickup offerings for residents, we need to parse these out from the GIS data
//and only reutrn the services (with the interval) that is scheduled.
function get_pickups_with_rules($desc, $service, $rules) {
    $p = strtoupper($desc);
    $s = strtoupper($service);
    $bins = array();

    foreach($rules as $rule) {
        if (str_contains($p, strtoupper($rule["column_desc"])) && str_contains($s, strtoupper($rule["column_garb"]))) {
            if (!empty($rule["waste_start"])) {
                $bins[] =  array(
                    'PickupName' => 'WasteBin',
                    'Start' => $rule["waste_start"],
                    'Interval' => intval($rule["waste_cadence"])
                );
            }
            if (!empty($rule["green_start"])) {
                $bins[] =  array(
                    'PickupName' => 'GreenBin',
                    'Start' => $rule["green_start"],
                    'Interval' => intval($rule["green_cadence"])
                );
            }
            if (!empty($rule["recycle_start"])) {
                $bins[] =  array(
                    'PickupName' => 'RecycleBin',
                    'Start' => $rule["recycle_start"],
                    'Interval' => intval($rule["recycle_cadence"])
                );
            }
            break;
        }
    }
    return $bins;
}

function generate($input_file, $output_file, $ruleset) {
    $csv = array_map(function($v){return str_getcsv($v, ',', '"', "");}, file($input_file));
    $err_cnt = 0;
    array_walk($csv, function(&$a) use ($csv) {
        $a = array_combine($csv[0], $a);
    });
    array_shift($csv); # remove column header

    $rules_raw = file_get_contents($ruleset);
    $rules = json_decode($rules_raw, true);

    $line = 0;
    foreach ($csv as &$value) {
        
        $line++; //header

        if (!validate_input($value, $line, $output_file)) {
            //error is printed by above function
            $err_cnt++;
            continue;
        }

        $record = array();
        $poscode = get_postcode($value[Constants::COLUMN_NAME_SUBURB]);
        if ($poscode != 0) {
            $record['PostCode'] = $poscode;
        }
        $record['Suburb'] = $value[Constants::COLUMN_NAME_SUBURB];
        $record['Street'] = $value[Constants::COLUMN_NAME_STREET_NAME];
        $record['Number'] = $value[Constants::COLUMN_NAME_STREET_NUMBER];
        if (Constants::USE_STREET_NUMBER_PARTS) {
            $unit = $value[Constants::COLUMN_NAME_STREET_UNIT];
            if (is_numeric($unit)) {
                $record['Number'] = $unit . "/" . $record['Number'];
            }
        }
        $record['Pickups'] = array();

        $dow = get_dow($value[Constants::COLUMN_NAME_SERVICE]);
        if ($dow > 0) {
            $pickups = get_pickups_with_rules($value[Constants::COLUMN_NAME_SERVICE_DESC], $value[Constants::COLUMN_NAME_SERVICE], $rules);
            $record['Pickups'] = array_filter($pickups);
        }

        $valid = validate_row($record);
        if ($valid == null) {
            fwrite($output_file, json_encode($record, JSON_UNESCAPED_SLASHES) . "\r\n");
        } else {
            $err_cnt++;
            if (Constants::OUTPUT_ERRORS) {
                fwrite($output_file, "# Error line " . $line . ": " . $valid . " - " . json_encode($record) . "\r\n");
            }
        } 
    }
    fwrite($output_file, "# File processed with rules " . basename($ruleset) . "\r\n");
    return $err_cnt;
}


if(defined('STDIN') ) {
    if (!file_exists(Constants::INPUT_FILE_NAME)) {
        die("Cannot find input file");
    }
    $stdout = fopen('php://stdout', 'w');
    generate(Constants::INPUT_FILE_NAME, $stdout, 'rules/rules.json');
}
